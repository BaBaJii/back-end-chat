const express = require("express");
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const connect = require("./db");
const User = require("./userschema");
const { db } = require("./userschema");
const { data } = require("jquery");

const router = express.Router();

router.get('/getLogin', function (req, res, next) {
  User.find({}, function(err, Data) { 
    if(err) {
        console.log(err);
        return res.status(500).send();
    } else {
        return res.status(200).send(Data);
    }
});
});

router.post('/addUser', function (req, res, next) {
  // console.log("=====reqData========>", req.body)  
  

  User.findOne({"username":req.body.username}, function (err, result) {
    // console.log('respones',result)
    // console.log('res',err)
    if (result) {
      res.send(result)
    } else {
      const data = new User(req.body);
      return data.save()    
        .then(data => {
          console.log('after save',data)
          res.send(data)
        })
        .catch(err => {
          res.status(400).send("unable to save")
        })
    }
  });
});


module.exports = router;





