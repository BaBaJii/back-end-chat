var express = require('express');
var app = express();
var cors = require('cors')
const index = require('../routes/index')
const http = require("http");
const socketIo = require("socket.io");
const port = process.env.PORT || 7088;
const server = require("http").Server(app);
const io = require("socket.io")(server);
const bodyParser = require("body-parser");


//connectiong database
const User = require("../login/userschema");
const connect = require("../login/db");
const Router = require("../login/routes");
const chatdb = require("../chat/chatroutes")


app.use(index)
app.use(cors())
app.use(express.static('public'))
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use("/getlogin", Router);
app.use('/add', Router)
app.use('/chatdata', chatdb)
app.use('/chat', chatdb)
app.use('/delete', chatdb)



//sockets

var client = {}
io.on("connection", socket => {

  // const { id } = socket.client;
  // client.push(id)
  // socket.on("chat message",  ({userid, msg})  => {
  //   console.log(`${id}: ${msg}`);
  // io.emit("chat message", { userid, msg }) 
  // console.log("socket-id", client)


  socket.on('new-user', userid => {
    client[userid] = socket.id;
    console.log(client)
  })

  socket.on('private-message', ({ selected_user, msg, userid }) => {

    socket.join(client)

    var id = client[selected_user]
    // console.log("reciver-id", id)

    var us = client[userid]
    // console.log("sender-id", us)

    // console.log("Sending: " + msg + " to " + selected_user.username);
    // console.log("filessss",files)


    io.to(us).emit("private-message", { selected_user, msg, userid });
    io.to(id).emit("recived-message", { selected_user, msg, userid });

    // console.log("user name-",selected_user.username,"/socket-id-",id)  

    // console.log(userid, "sender name")
  });
  socket.on("image", function ({ img, userid, selected_user,image,video }) {

    socket.join(client)
    var id = client[selected_user]
    var us = client[userid]
    // console.log(image)

    // console.log(img)  
    io.to(us).emit("addimage", { img, userid , image, video });
    io.to(id).emit("image-recived", { img, userid, image, video  });
  });

  
});

server.listen(port, () => console.log(`Listening on port ${port}`));

