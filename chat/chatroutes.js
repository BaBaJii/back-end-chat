const express = require("express");
var app = express();
const paginate = require("jw-paginate");
const { ObjectID } = require('mongodb');

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const connect = require("../login/db");
const Chat = require("./chatschema");
const { then } = require("../login/db");
const { Mongoose } = require("mongoose");

const chatdb = express.Router();

chatdb.post("/chat", (req, res, next) => {
  // console.log("-----", req.body)
  try {
    var chat = new Chat(req.body);
    chat
      .save()
      .then((data) => {
        // console.log('after save', data)
        // res.send(data)
      })
      .catch((err) => {
        console.log(err);
      });
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(500);
    console.error(error);
  }
});

chatdb.delete("/delete/:id", (req, res, next) => {
  console.log("++++++++",req.params.id)
  Chat.findByIdAndRemove({ _id: req.params.id}, (err, result) => {
    if(err){
      throw err;
    }
    else{
    res.send(result)}
  });
});



chatdb.get("/userchat", function (req, res, next) {
  console.log("======================", req.query)
    const userid = req.query.user;
  const selected = req.query.selected;

  const pageOptions = {
    page: parseInt(req.query.page) || 0,
    limit: parseInt(req.query.limit) ||20
}
console.log(pageOptions)
Chat.find(  { reciver: [selected, userid], userid: [selected, userid] }).skip(pageOptions.page * pageOptions.limit)
  .limit(pageOptions.limit).exec(function (err, Data) {
    // console.log("db data",Data)
  
    if (err) {
            console.log(err);
            return res.status(500).send();
          } else {
            Chat.count((error,numOfDocs)=>{
              if(error){return res.status(500).send();}
              else{
                let data1={
                  data:Data,
                  totalCount:numOfDocs
                }
                console.log(data1,"===========")
                return res.status(200).json(data1);
              }
            })
           
          }
        })
})


// chatdb.get("/userchat", function (req, res, next) {
//   console.log("======================", req.query)

//   const userid = req.query.user;
//   const selected = req.query.selected;
//   Chat.find(
//     { reciver: [selected, userid], userid: [selected, userid] },
//     function (err, Data) {
//       // console.log('===>', Data)
//       if (err) {
//         console.log(err);
//         return res.status(500).send();
//       } else {
//         return res.status(200).json(Data);
//       }
//     }
//   );
// });

module.exports = chatdb;
